var DSSV = [];
//laays dữ liệu từ lacalstorage khi user load trang
var dataJson = localStorage.getItem("DSSV_LOCAL");
if (dataJson != null) {
  var dataArr = JSON.parse(dataJson);
  //dl laays len từ local sex ko có method tinhsDTB vì vậy ta convert thành arr có method bằng map
  DSSV = dataArr.map(function (item) {
    var sv = new SinhVien(
      item.maSV,
      item.tenSV,
      item.emailSV,
      item.pass,
      item.diemToan,
      item.diemLy,
      item.diemHoa
    );
    return sv;
  });
}

//thêm sv

function themSV() {
  var sv = layThongTin();
  //validate kt dl
  kiemTraTrung(sv.maSV, DSSV);

  //push sv vaof dssv
  DSSV.push(sv);
  //convert aray dssv thành json
  var dssvJson = JSON.stringify(DSSV);
  //lưu json vào local storege
  localStorage.setItem("DSSV_LOCAL", dssvJson);
  renderDSSV(DSSV);
}
//xoas sv
function xoaSV(idSV) {
  var viTri = timKiemViTri(idSV, DSSV);
  if (viTri != -1) {
    DSSV.splice(viTri, 1);
    renderDSSV(DSSV);
  }
}

//sua sv
function suaSV(idSV) {
  var viTri = timKiemViTri(idSV, DSSV);
  if (viTri != -1) {
    var sv = DSSV[viTri];
    console.log(sv);
    document.getElementById("txtMaSV").disabled = true;
  }
}
