function layThongTin() {
  //laays thong tin tu form
  var _maSV = document.getElementById("txtMaSV").value;
  var _tenSV = document.getElementById("txtTenSV").value;
  var _emailSV = document.getElementById("txtEmail").value;
  var _matKhauSV = document.getElementById("txtPass").value;
  var _diemToan = document.getElementById("txtDiemToan").value * 1;
  var _diemLy = document.getElementById("txtDiemLy").value * 1;
  var _diemHoa = document.getElementById("txtDiemHoa").value * 1;
  //taoj objec sv
  return new SinhVien(
    _maSV,
    _tenSV,
    _emailSV,
    _matKhauSV,
    _diemToan,
    _diemLy,
    _diemHoa
  );
}
function renderDSSV(svArr) {
  //render DSsV
  //   content :chuoi chua the tr
  var content = "";
  for (let index = 0; index < svArr.length; index++) {
    var sv = svArr[index];
    var contentTr = `<tr>
                        <td>${sv.maSV}</td>
                        <td>${sv.tenSV}</td>
                        <td>${sv.emailSV}</td>
                        <td>${sv.tinhDTB()}</td>
                        <td>
                        <button class="btn btn-success" onclick=" xoaSV('${
                          sv.maSV
                        }')">Xóa</button>
                        <button class="btn btn-success" onclick=" suaSV('${
                          sv.maSV
                        }')">Sửa</button></td>
                     </tr>`;
    content += contentTr;
  }
  document.getElementById("tbodySinhVien").innerHTML = `${content}`;
}
function timKiemViTri(id, arr) {
  var viTri = -1;
  for (let index = 0; index < arr.length; index++) {
    if (arr[index].maSV == id) {
      viTri = index;
    }
  }
  return viTri;
}
